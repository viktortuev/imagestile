﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImagesStoryboard
{
    public class Container
    {
        [JsonProperty(PropertyName = "imageurl")]
        public string ImageUrl { get; set; }

        [JsonProperty(PropertyName = "subcontainers")]
        public List<Container> SubContainers { get; set; }

        [JsonProperty(PropertyName = "isrow")]
        public bool IsRow { get; set; }

        public Container()
        {
            SubContainers = new List<Container>();
        }
    }
}
