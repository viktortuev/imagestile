﻿$(function () {
    var imageContainer = $('.js-images-container')[0],
        images = JSON.parse(imagesJson),
        loaders = [],
        params;
    
    var drawStoryboard = function (imageTree, drawParams) {
        params = drawParams;
        loadImages(imageTree);
        $.when.apply(null, loaders).done(function () {
            resize(imageTree);
            drawNode(imageTree, params.paddingLeft, params.paddingTop);
        })
    };

    var drawNode = function (node, x, y) {
        var container = $('<div class="image-container ' + (node.isrow ? 'row' : 'col') + '" style="left: ' + x + 'px; top: ' + y + 'px;">'
            + '</div>')[0];
        if (!node.imageurl) {
            var coordX = x,
                coordY = y;
            node.subcontainers.forEach(function (item, i) {
                container.appendChild(drawNode(item, coordX, coordY));
                if (node.isrow) {
                    coordX += Math.floor(item.width);
                }
                else {
                    coordY += Math.floor(item.height);
                }
            })
        }
        else {
            node.image.width = Math.max(Math.floor(getImageWidth(node.image)), 1);
            node.image.height = Math.max(Math.floor(getImageHeight(node.image)), 1);
            node.image.style.left = x + 'px';
            node.image.style.top = y + 'px';
            imageContainer.appendChild(node.image);
        }
        return container;
    };

    function loadImages(node) {
        if (!node.imageurl) {
            node.subcontainers.forEach(function (item, i) {
                loadImages(item);
            })
        }
        else {
            var deferred = $.Deferred();
            node.image = new Image();
            node.image.src = node.imageurl;
            node.image.onload = function(){
                deferred.resolve();
            }
            loaders.push(deferred.promise());
        }
    }

    function resize(node) {
        toSmallImages(node);

        while (node.width < params.width) {
            inscribeImage(node);
            node.height += 1;
        }
    }

    function inscribeImage(node) {
        if (!node.imageurl) {
            if (node.isrow && getSumWidth(node.subcontainers) < node.width) {
                node.height += .3;
            }
            if (!node.isrow && getSumHeight(node.subcontainers) < node.height) {
                node.width += .3;
            }
            node.subcontainers.forEach(function (item, i) {
                if (node.isrow && node.height > item.height) {
                    item.height = node.height;
                }
                if (!node.isrow && node.width > item.width) {
                    item.width = node.width;
                }
                
                inscribeImage(item);
            })
            var resultWidth = getSumWidth(node.subcontainers),
                resultHeight = getSumHeight(node.subcontainers);
            if (node.isrow && node.width < resultWidth) {
                node.width = resultWidth;
            }
            if (!node.isrow && node.height < resultHeight) {
                node.height = resultHeight;
            }
            if (node.isrow && node.width > resultWidth || !node.isrow && node.height > resultHeight) {
                inscribeImage(node);
            }
        }
        else {
            var ratio = getRatio(node.image);
            if (node.image.dataWidth < node.width) {
                setImageWidth(node.image, node.width);
            }
            if (node.image.dataHeight < node.height) {
                setImageHeight(node.image, node.height);
            }
            if (node.width < node.image.dataWidth) {
                node.width = node.image.dataWidth;
            }
            if (node.height < node.image.dataHeight) {
                node.height = node.image.dataHeight;
            }
        }
    }

    function getSumWidth(containers) {
        var summWidth = 0;
        containers.forEach(function (item, i) {
            summWidth += item.width;
        });
        return summWidth;
    }

    function getSumHeight(containers) {
        var summHeight = 0;
        containers.forEach(function (item, i) {
            summHeight += item.height;
        });
        return summHeight;
    }

    function toSmallImages(node) {
        var nodeWidth = 0,
            nodeHeight = 0;
        if (!node.imageurl) {
            node.subcontainers.forEach(function (item, i) {
                toSmallImages(item);
                if (node.isrow) {
                    nodeWidth = nodeWidth + item.width;
                    nodeHeight = Math.max(nodeHeight, item.height);
                }
                else {
                    nodeWidth = Math.max(nodeWidth, item.width);
                    nodeHeight = nodeHeight + item.height;
                }
            });
        }
        else {
            var maxSize = 1;
            node.image.dataWidth = node.image.width;
            node.image.dataHeight = node.image.height;
            if (node.image.dataWidth > maxSize || node.image.dataHeight > maxSize) {
                var ratio = getRatio(node.image);
                if (ratio >= 1) {
                    setImageWidth(node.image, maxSize + params.paddingLeft + params.paddingRight);
                }
                else {
                    setImageHeight(node.image, maxSize + params.paddingTop + params.paddingBottom);
                }
            }
            nodeWidth = node.image.dataWidth;
            nodeHeight = node.image.dataHeight;
        }
        node.width = nodeWidth;
        node.height = nodeHeight;
    }
    
    function getRatio(image) {
        if (image.ratio) {
            return image.ratio;
        }
        image.ratio = image.width / image.height;
        return image.ratio;
    }

    function setImageWidth(image, width) {
        var ratio = getRatio(image),
            imageWidth = width - params.paddingLeft - params.paddingRight,
            imageHeight = imageWidth / ratio;
        image.dataWidth = imageWidth + params.paddingLeft + params.paddingRight;
        image.dataHeight = imageHeight + params.paddingTop + params.paddingBottom;
    }

    function setImageHeight(image, height) {
        var ratio = getRatio(image),
            imageHeight = height - params.paddingTop - params.paddingBottom,
            imageWidth = imageHeight * ratio;
        image.dataWidth = imageWidth + params.paddingLeft + params.paddingRight;
        image.dataHeight = imageHeight + params.paddingTop + params.paddingBottom;
    }

    function getImageWidth(image) {
        return image.dataWidth - params.paddingLeft - params.paddingRight;
    }

    function getImageHeight(image) {
        return image.dataHeight - params.paddingTop - params.paddingBottom;
    }
    
    drawStoryboard(images, {
        width: 1000,
        //paddingTop: 2, paddingRight: 2,
        //paddingBottom: 3, paddingLeft: 5
        paddingTop: 5, paddingRight: 5,
         paddingBottom: 5, paddingLeft: 5
        //paddingTop: 0, paddingRight: 0,
        //paddingBottom: 50, paddingLeft: 0
    })
})