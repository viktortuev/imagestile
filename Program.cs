﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImagesStoryboard
{
    class Program
    {
        private static Random Gen = new Random();
        private static string[] Images = Directory.GetFiles("../../images/").Select(x => x.Substring(6)).ToArray();
        private const string JsonPath = "../../ImagesTree.js";

        static void Main(string[] args)
        {
            //var images = Directory.GetFiles("../../images/");
            var tree = new Container();
            tree.IsRow = true;
            Fill(tree, 2);
            var serialized = JsonConvert.SerializeObject(tree);
            using (StreamWriter sw = File.CreateText(JsonPath))
            {
                sw.Write("var imagesJson = '" + serialized + "'");
            }
        }

        private static void Fill(Container container, int imageChance)
        {
            var isImage = Gen.Next(11) < imageChance;
            
            if (isImage)
            {
                var imageIndex = Gen.Next(Images.Length - 1);
                container.ImageUrl = Images[imageIndex];
            }
            else
            {
                var subContainersCount = Gen.Next(2, 5);

                for (var i = 0; i < subContainersCount; i++)
                {
                    var newContainer = new Container();
                    newContainer.IsRow = !container.IsRow;
                    Fill(newContainer, imageChance + 2);
                    container.SubContainers.Add(newContainer);
                }
            }
        }
    }
}
