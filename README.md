To view images board open Index.html in a browser.

You could change board width and paddings in the ImageController.js
```
#!javascript
    drawStoryboard(images, {
        width: 1000,
        paddingTop: 2, paddingRight: 2,
        paddingBottom: 3, paddingLeft: 5
    })
```

Also you could generate images tree by invoke console project ImagesStoryboard